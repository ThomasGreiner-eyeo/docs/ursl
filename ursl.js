"use strict";

function parseABP(content) {
  let rules = [];
  let lines = content.split("\n");
  
  for (let line of lines) {
    line = line.trim();
    
    if (!line) {
      rules.push(null);
      continue;
    }
    
    let match = /^!(.*)$/.exec(line);
    if (match) {
      rules.push({
        text: match[1],
        type: "comment"
      });
      continue;
    }
    
    match = /^\[(.*)\]\s*$/.exec(line);
    if (match) {
      rules.push({
        text: ` Requires: ${match[1]}`,
        type: "comment"
      });
      continue;
    }
    
    match = /^([^#]*)#(@?)#(.*)$/.exec(line);
    if (match) {
      let [, hosts, exception, pattern] = match;
      rules.push({
        isException: !!exception,
        pattern,
        syntax: "css",
        target: "dom",
        hosts: hosts.split(","),
        reqTypes: new Map()
      });
      continue;
    }
    
    let [, exception, pattern, options] = /^(@@)?([^\$]*)(?:\$(.*))?$/.exec(line);
    let rule = {
      isException: !!exception,
      pattern,
      syntax: "abp",
      target: "url",
      hosts: [],
      reqTypes: new Map()
    };
    if (options) {
      options = options.split(",");
      for (let option of options) {
        let [, key, value] = /^([^=]+)(?:=(.*))?$/.exec(option);
        if (key == "domain") {
          rule.hosts = value.split("|");
        } else {
          let value = (key[0] != "~");
          rule.reqTypes.set((value) ? key : key.substr(1), value)
        }
      }
    }
    rules.push(rule);
  }
  
  return rules;
}

function serializeURSL(rules) {
  let lines = [];
  
  for (let rule of rules) {
    if (!rule) {
      lines.push("");
      continue;
    }
    
    if (rule.type == "comment") {
      lines.push(`//${rule.text}`);
      continue;
    }
    
    let line = (rule.isException) ? "allow" : "deny";
    lines.push(`${line} ${rule.syntax}(${rule.target}):${rule.pattern}`);
    
    if (rule.hosts.length > 0) {
      lines.push(`- host: ${rule.hosts.join(", ")}`);
    }
    
    for (let [key, value] of rule.reqTypes) {
      lines.push(`- ${key}: ${value}`);
    }
  }
  
  return lines;
}

function fromABP(content) {
  let rules = parseABP(content);
  return serializeURSL(rules);
}
exports.fromABP = fromABP;

function parseURSL(content) {
  let rules = [];
  let lines = content.split("\n");
  
  for (let line of lines) {
    line = line.trim();
    
    if (!line) {
      rules.push(null);
      continue;
    }
    
    let match = /^\/\/(.*)$/.exec(line);
    if (match) {
      rules.push({
        text: match[1],
        type: "comment"
      });
      continue;
    }
    
    match = /^(allow|deny|disable|-)\s+(.*)$/.exec(line);
    if (!match)
      continue;
    
    let [, cmd, args] = match;
    if (cmd == "-") {
      let [, key, values] = /^([a-z\-]+)\s*:\s*(.*)$/.exec(args);
      let rule = rules[rules.length - 1];
      
      if (key == "host") {
        let hosts = values.split(/\s*,\s*/);
        rule.hosts = rule.hosts.concat(hosts);
      } else {
        rule.reqTypes.set(key, values == "true");
      }
      continue;
    }
    
    let [, syntax, target, pattern] = /^([^\(]+)\(([^\)]+)\):(.*)$/.exec(args);
    rules.push({
      isException: cmd == "allow" || cmd == "disable",
      pattern, syntax, target,
      hosts: [],
      reqTypes: new Map(),
      type: "statement"
    });
  }
  
  return rules;
}

function serializeABP(rules) {
  return rules.map((rule) => {
    let filter = "";
    
    if (!rule)
      return filter;
    
    if (rule.type == "comment")
      return `!${rule.text}`;
    
    if (rule.syntax == "css") {
      filter = (rule.isException) ? "#@#" : "##";
      filter = `${rule.hosts.join(",")}${filter}${rule.pattern}`;
    } else {
      if (rule.isException) {
        filter = "@@";
      }
      filter += rule.pattern;
      
      if (rule.hosts.length > 0 || rule.reqTypes.size > 0) {
        filter += "$";
        
        let options = [];
        if (rule.hosts.length > 0) {
          options.push(`domain=${rule.hosts.join("|")}`);
        }
        for (let [key, value] of rule.reqTypes) {
          options.push((value) ? key : `~${key}`);
        }
        filter += options.join(",");
      }
    }
    
    return filter;
  });
}

function toABP(content) {
  let rules = parseURSL(content);
  return serializeABP(rules);
}
exports.toABP = toABP;

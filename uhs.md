# UHS - Uniform Host Selector

## Goals

- Select one or more hosts or sub hosts
- Select which hosts should be matched or not matched

## Syntax

```
uhs = hostpart *( "." hostpart )
  hostpart = single / multiple / string
    multiple = "(" uhs *( "," uhs ) ")"
    single = ( "+" / "-" ) uhs / uhs / ( "+" / "-" )
```

## Examples

Match all subdomains of domain foo.com, foo.net and foo.org including the root domain but excluding the subdomain "www":

`(+,-www).+foo.(com,net,org)`

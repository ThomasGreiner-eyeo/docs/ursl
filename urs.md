# URS - Uniform Resource Selector

## Goals

- Select any resource using arbitrary single-line patterns

## Features

- syntax
  - abp: Adblock Plus-style pattern
  - css: CSS selector
  - regex: regular expression
  - xpath: XPath
- target
  - css: CSS rule
  - dom: DOM element
  - url: URL

## Syntax

```
urs = prefix ":" pattern
  prefix = <couldn't decide on one yet so see below>
    syntax "(" target ")"
    target "." syntax
    target "/" syntax
    syntax = "abp" / "css" / "regex" / "xpath"
    target = "dom" / "css" / "url"
  pattern = <depending on pattern syntax>
```

## Examples

Select all DOM elements with the ID "foo" from using a CSS selector:

`css(dom):#foo`

Select all HTTP requests with the URL matching a regular expression:

`regex(url):[\?&]foo=\d+&`

Select all HTTP requests with the URL matching an Adblock Plus-style pattern:

`abp(url):||foo.com/*/bar.js^`

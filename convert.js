#!/usr/bin/env node

"use strict";

const fs = require("fs");

const ursl = require("./ursl");

let [,, method, filename] = process.argv;

fs.readFile(filename, "utf8", (err, content) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  
  if (method == "abp2ursl") {
    console.log(ursl.fromABP(content).join("\n"));
  } else if (method == "ursl2abp") {
    console.log(ursl.toABP(content).join("\n"));
  }
});

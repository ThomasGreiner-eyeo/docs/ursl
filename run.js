#!/usr/bin/env node

"use strict";

const ursl = require("./ursl");

// shuf -n 10 exceptionrules.txt
let abpCode = `
! almodi.org ads (https://adblockplus.org/forum/viewtopic.php?f=12&t=42486)
@@|https://$popup,domain=search.sidecubes.com
@@||google.gm/ads/user-lists/$image,subdocument,third-party
! @@||imp.admarketplace.net^$third-party
@@||optimized-by.rubiconproject.com/a/11384/31484/197696-15.js$domain=googlesyndication.com|reuters.com
order-order.com#@#.mid-bar-post-container-unit
rambler.ru#@#div[class^="adfox-ban_"]
! @@||g.doubleclick.net/pagead/ads?*^slotname=5173765237%2F8127180997^$subdocument,domain=axlegeeks.com|credio.com|findthecompany.com|findthedata.com|findthehome.com|gearsuite.com|healthgrove.com|homeowl.com|insidegov.com|mooseroots.com|petbreeds.com|pointafter.com|softwareinsider.com|specout.com|startclass.com|underthelabel.com|wanderbat.com|weatherdb.com
hdtrailer.ru#@#.ya-partner
@@||maps.google.mx^$~third-party
`.trim();

let urslCode = `
allow abp(url):|https://
- popup: true
- host: search.sidecubes.com
allow abp(url):||google.gm/ads/user-lists/
- subdocument: true
- third-party: true
allow abp(url):||optimized-by.rubiconproject.com/a/11384/31484/197696-15.js
- host: googlesyndication.com, reuters.com
allow css(dom):.mid-bar-post-container-unit
- host: order-order.com
allow css(dom):div[class^="adfox-ban_"]
- host: rambler.ru
allow css(dom):.ya-partner
- host: hdtrailer.ru
allow abp(url):||maps.google.mx^
- third-party: false
`.trim();

console.log(ursl.fromABP(abpCode));
console.log(ursl.toABP(urslCode));

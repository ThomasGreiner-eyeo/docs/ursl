# URSL - Uniform Resource Selector Language

The URSL proposal consists of the following parts:

1. [URSL](ursl.md) to apply rules based on URS and UHS patterns (~ Adblock Plus Filter Lists)
2. [URS](urs.md) to match resources (~ Adblock Plus Filters)
3. [UHS](uhs.md) to match domains (~ domains list in Adblock Plus Filters)

## Goals

- Split up indepdendent parts which are contained within Adblock Plus Filters
- Provide consistent and forwards-compatible way to provide functionality to ad blocking software
- Compile to/from Adblock Plus filter syntax for backwards compatibility
- Allow for more structured development workflow
- Keep existing simplicity and keep it declarative

## Features

- meta data
  - minimum version
  - expiration
  - checksum
  - redirect
  - version
  - homepage
  - title
- comments
- logic
  - allow (i.e. to whitelist)
  - deny (i.e. to block)
  - disable (e.g. disabling elemhide)
- options
  - flags (e.g. request types)
  - host (i.e. domains)
  - key (i.e. sitekeys)
- placeholders in patterns for domain(?)
  - variables(?)
  - control structures(?)

## Syntax

```
ursl = *( statement "\n" )
  statement = command urs / "-" option ...
  command = "allow" / "deny" / "disable" feature
    feature = "document" / elemhide" / "genericblock" / "generichide"
  option = optionkey ":" optionvalues
    optionkey = "flag" / "host" / "key"
    optionvalues = optionvalue *( "," optionvalue )
      optionvalue = [ "!" ] <depending on option type>
```

## Concepts

Before

`@@||foo.com/*/bar.js$domain=foo.com|bar.com|~www.foo.com,~subdocument,third-party`

After

```
allow abp(url):||foo.com/*/bar.js
- host: (+bar,-www.+foo).com
- flags: -subdocument, third-party
```

### Introduce command keywords

**`allow `**`||foo.com/*/bar.js$domain=foo.com|bar.com|~www.foo.com,~subdocument,third-party`

This change separates the selector which describes the element from the action which describes what to do with the element.

### Separate options

```
@@||foo.com/*/bar.js
- domain: foo.com|bar.com|~www.foo.com
- subdocument: false
- third-party: true
```

This change allows for higher flexibility in regards to development and forwards-compatibility. It also allows for cascading and makes blocking and hiding filters consistent.

### Restructure options

`@@||foo.com/*/bar.js$`**`host`**`=foo.com|bar.com|`**`-`**`www.foo.com,`**`flags`**`=`**`-`**`subdocument|third-party`

This changes groups binary options together and makes options consistent with other changes.

### Introduce URS

**`abp(url):`**`@@||foo.com/*/bar.js$domain=foo.com|bar.com|~www.foo.com,~subdocument,third-party`

This change eliminates confusion around what is selected and which syntax is used. This would usually require finding certain indicators in the line. By eliminating that it is capable of specifying any target and any pattern without introducing new syntax.

### Introduce UHS

`@@||foo.com/*/bar.js$domain=`**`(+bar,-www.+foo).com`**`,~subdocument,third-party`

This change allows domains to be specified in a more compact and less ambiguous way.

## Examples

Unblock an HTTP request with a URL matching an Adblock Plus-style pattern on selected domains:

URSL
```
allow abp(url):||foo.com/*/bar.js
- host: +.+(foo,bar).(ae,ba,bg,biz,ch,co.id,co.il,co.in,co.ke,co.kr,co.nz,co.th,co.za,com.au,com.bo,com.es,com.hk,com.mx,com.my,com.ng,com.ph,com.sg,com.tw,com.ua,com.vn,dk,ee,eu,fi,gr,hk,hr,hu,ie,in,info,is,jp,kr,lt,lv,my,net,no,org,ph,pk,ro,rs,sa,se,sg,si,tw,ua,us,vn,wiki,ws)
- host: +.+(foodomain,bardomain).com
- host: +.+baz.(com,org)
```

Adblock Plus filter
```
@@||foo.com/*/bar.js$domain=foo.ae|foo.ba|foo.bg|foo.biz|foo.ch|foo.co.id|foo.co.il|foo.co.in|foo.co.ke|foo.co.kr|foo.co.nz|foo.co.th|foo.co.za|foo.com.au|foo.com.bo|foo.com.es|foo.com.hk|foo.com.mx|foo.com.my|foo.com.ng|foo.com.ph|foo.com.sg|foo.com.tw|foo.com.ua|foo.com.vn|foo.dk|foo.ee|foo.eu|foo.fi|foo.gr|foo.hk|foo.hr|foo.hu|foo.ie|foo.in|foo.info|foo.is|foo.jp|foo.kr|foo.lt|foo.lv|foo.my|foo.net|foo.no|foo.org|foo.ph|foo.pk|foo.ro|foo.rs|foo.sa|foo.se|foo.sg|foo.si|foo.tw|foo.ua|foo.us|foo.vn|foo.wiki|foo.ws|foodomain.com|baz.com|baz.org|bar.ae|bar.at|bar.ba|bar.bg|bar.biz|bar.ca|bar.ch|bar.cl|bar.co.id|bar.co.il|bar.co.in|bar.co.ke|bar.co.kr|bar.co.nz|bar.co.th|bar.co.uk|bar.co.ve|bar.co.za|bar.com|bar.com.ar|bar.com.au|bar.com.bo|bar.com.br|bar.com.co|bar.com.es|bar.com.hk|bar.com.mx|bar.com.my|bar.com.ng|bar.com.ph|bar.com.pl|bar.com.sg|bar.com.tr|bar.com.tw|bar.com.ua|bar.com.vn|bar.cz|bar.de|bar.dk|bar.do|bar.ee|bar.es|bar.eu|bar.fi|bar.fr|bar.gr|bar.hk|bar.hr|bar.hu|bar.ie|bar.is|bar.it|bar.jp|bar.kr|bar.lt|bar.lv|bar.mx|bar.my|bar.ng|bar.nl|bar.no|bar.org|bar.pg|bar.ph|bar.pk|bar.pt|bar.ro|bar.rs|bar.ru|bar.sa|bar.se|bar.sg|bar.si|bar.sk|bar.tw|bar.ua|bar.vn|bar.wiki|bar.ws|bardomain.com
```
